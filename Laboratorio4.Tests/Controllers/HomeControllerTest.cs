﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Laboratorio4.Controllers;
using System.Web.Mvc;
namespace Laboratorio4.Tests.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        [TestMethod]
        public void ContactSendValuesTest()
        {
            //Arrange
            HomeController homeController = new HomeController();
            //Act
            ViewResult vista = homeController.Contact() as ViewResult;
            //Assert
            Assert.IsNotNull(vista);
            Assert.AreEqual("Contact", vista.ViewName);
            Assert.AreEqual("Your contact page.", vista.ViewBag.Message);
            Assert.AreEqual(1, vista.ViewBag.ValorParaTest);
        }

        [TestMethod]
        public void AboutMessageTest()
        {
            //Arrange
            HomeController homeController = new HomeController();
            //Act
            ViewResult vista = homeController.About() as ViewResult;
            //Assert
            Assert.IsTrue("Este es un mensaje para el test." == vista.ViewBag.Message);
        }
    }
}