﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
namespace Laboratorio4.Tests.UITesting
{
    [TestClass]
    public class Selenium
    {
        IWebDriver driver;
        [TestMethod]
        public void PruebaListarPlanetas()
        {
            ///Arrange 
            /// Crea el driver de Chrome 
            driver = new ChromeDriver();
            string URL = "https://localhost:44371/";

            /// Pone la pantalla en full screen 
            driver.Manage().Window.Maximize();
            ///Act 
            /// Se va a la URL indicada 
            driver.Url = URL;
            IWebElement enlaceListarPlaneta = driver.FindElement(By.Id("test"));
            enlaceListarPlaneta.Click();
            IWebElement h1 = driver.FindElement(By.Id("tituloTest"));
            ///Assert 
            Assert.AreEqual("Tabla de Planetas", h1.Text);
        }

        [TestCleanup]
        public void TearDown()
        {
            if (driver != null)
            {
                driver.Quit();
            }
        }
    }
}